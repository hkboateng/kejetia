package com.protecksoftware.kejetia.application.request;


public class AgentProspectiveRequest{
	private String agentNumber;
	
	private int prospectiveId;
	
	private String insuranceType;
	
	private int agentId;

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public int getProspectiveId() {
		return prospectiveId;
	}

	public void setProspectiveId(int prospectiveId) {
		this.prospectiveId = prospectiveId;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	
	
	
}
