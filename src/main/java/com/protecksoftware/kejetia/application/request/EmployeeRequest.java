package com.protecksoftware.kejetia.application.request;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeRequest{
	private String requestType;
	
	private final String requestId;
	
	private final String address1;

	private String address2;

	private String city;

	private int companyId;

	private String emailAddress;

	private final String firstname;

	private String gender;

	private final String lastname;

	private String middlename;

	private final String phoneNumber;
	
	private String employeeType;
	
	/**
	 * Can be Business name or Agent full name
	 */
	private String agentName;
	
	@JsonProperty("state")
	private String region;
	
	private int employeeId;


	public EmployeeRequest(String firstname, String lastname,String phoneNumber, String address1) {
		this.requestId = UUID.randomUUID().toString();
		this.address1 = address1;
		this.firstname = firstname;
		this.lastname = lastname;
		this.phoneNumber = phoneNumber;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRequestId() {
		return requestId;
	}

	public String getAddress1() {
		return address1;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	
	
}
