package com.protecksoftware.kejetia.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the agentInsuranceDetails database table.
 * 
 */
@Entity
@Table(name="agentInsuranceDetails")
@NamedQuery(name="AgentInsuranceDetail.findAll", query="SELECT a FROM AgentInsuranceDetail a")
public class AgentInsuranceDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int agentInsuranceDetailsId;

	private Timestamp dateCreated;

	private String insuranceType;

	//bi-directional many-to-one association to InsuranceAgent
	@ManyToOne
	@JoinColumn(name="agentId")
	private InsuranceAgent insuranceAgent;

	public AgentInsuranceDetail() {
	}

	public int getAgentInsuranceDetailsId() {
		return this.agentInsuranceDetailsId;
	}

	public void setAgentInsuranceDetailsId(int agentInsuranceDetailsId) {
		this.agentInsuranceDetailsId = agentInsuranceDetailsId;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getInsuranceType() {
		return this.insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public InsuranceAgent getInsuranceAgent() {
		return this.insuranceAgent;
	}

	public void setInsuranceAgent(InsuranceAgent insuranceAgent) {
		this.insuranceAgent = insuranceAgent;
	}

}