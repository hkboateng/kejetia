package com.protecksoftware.kejetia.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the agent_prospect database table.
 * 
 */
@Entity
@Table(name="agentProspective")
@NamedQuery(name="AgentProspectiveInsured.findAll", query="SELECT a FROM AgentProspectiveInsured a")
public class AgentProspectiveInsured implements Serializable {
	private static final long serialVersionUID = 1L;

	@OneToOne
	@JoinColumn(name="agentId")
	private InsuranceAgent agentId;

	private Timestamp dateCreated;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int agentProspectId;

	//bi-directional many-to-one association to ProspectiveInsured
	@OneToOne
	@JoinColumn(name="prospectiveId")
	private ProspectiveInsured prospectiveInsured;

	public AgentProspectiveInsured() {
	}

	public InsuranceAgent getAgentNumber() {
		return this.agentId;
	}

	public void setAgentNumber(InsuranceAgent agentId) {
		this.agentId = agentId;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getEmployeeProspectId() {
		return this.agentProspectId;
	}

	public void setEmployeeProspectId(int employeeProspectId) {
		this.agentProspectId = employeeProspectId;
	}

	public ProspectiveInsured getProspectiveInsured() {
		return this.prospectiveInsured;
	}

	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}

	public InsuranceAgent getAgentId() {
		return agentId;
	}

	public void setAgentId(InsuranceAgent agentId) {
		this.agentId = agentId;
	}

	public int getAgentProspectId() {
		return agentProspectId;
	}

	public void setAgentProspectId(int agentProspectId) {
		this.agentProspectId = agentProspectId;
	}
	
	

}