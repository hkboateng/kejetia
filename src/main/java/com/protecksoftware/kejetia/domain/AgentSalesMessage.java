package com.protecksoftware.kejetia.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the agentSalesMessages database table.
 * 
 */

public class AgentSalesMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int agentSalesMessageId;

	private Timestamp dateCreated;

	private int fromEmployeeId;

	private Message message;

	public AgentSalesMessage() {
	}

	public int getAgentSalesMessageId() {
		return this.agentSalesMessageId;
	}

	public void setAgentSalesMessageId(int agentSalesMessageId) {
		this.agentSalesMessageId = agentSalesMessageId;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getFromEmployeeId() {
		return this.fromEmployeeId;
	}

	public void setFromEmployeeId(int fromEmployeeId) {
		this.fromEmployeeId = fromEmployeeId;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

}