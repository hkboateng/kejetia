package com.protecksoftware.kejetia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the insuranceAgent database table.
 * 
 */

@Entity
@Table(name="insuranceAgent")
@NamedQuery(name="InsuranceAgents.findAll", query="SELECT i FROM InsuranceAgent i")
public class InsuranceAgent implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int agentId;

	private String agentName;

	private String agentNumber;
	
	
	@OneToOne
	@JoinColumn(name="employeeId")
	private Employee employee;
	
	@OneToOne
	@JoinColumn(name="agentAddressId")
	private Address agentAddress;
	
	@OneToOne
	@JoinColumn(name="locationId")
	private Location location;

	//bi-directional many-to-one association to AgentInsuranceDetail
	@OneToMany(mappedBy="insuranceAgent", fetch=FetchType.EAGER)
	private List<AgentInsuranceDetail> agentInsuranceDetails;

	public InsuranceAgent() {
	}

	public int getAgentId() {
		return this.agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return this.agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentNumber() {
		return this.agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}


	public Address getAgentAddress() {
		return agentAddress;
	}

	public void setAgentAddress(Address agentAddress) {
		this.agentAddress = agentAddress;
	}
	
	public List<AgentInsuranceDetail> getAgentInsuranceDetails() {
		return this.agentInsuranceDetails;
	}

	public void setAgentInsuranceDetails(List<AgentInsuranceDetail> agentInsuranceDetails) {
		this.agentInsuranceDetails = agentInsuranceDetails;
	}

	public AgentInsuranceDetail addAgentInsuranceDetail(AgentInsuranceDetail agentInsuranceDetail) {
		getAgentInsuranceDetails().add(agentInsuranceDetail);
		agentInsuranceDetail.setInsuranceAgent(this);

		return agentInsuranceDetail;
	}

	public AgentInsuranceDetail removeAgentInsuranceDetail(AgentInsuranceDetail agentInsuranceDetail) {
		getAgentInsuranceDetails().remove(agentInsuranceDetail);
		agentInsuranceDetail.setInsuranceAgent(null);

		return agentInsuranceDetail;
	}
	

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(employee.getFirstname());
		if(employee.getMiddlename() !=null){
			builder.append(" ");
			builder.append(employee.getMiddlename());
		}
		builder.append(" ");
		builder.append(employee.getLastname());		
		return builder.toString();
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}