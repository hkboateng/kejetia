package com.protecksoftware.kejetia.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;



/**
 * The persistent class for the insuredState database table.
 * 
 */
@Entity
@Table(name="insuredState")
@NamedQuery(name="InsuredState.findAll", query="SELECT i FROM InsuredState i")
public class InsuredState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int insuredStateId;

	private String currentState;

	private String dateChanged;

	@OneToOne
	@JoinColumn(name="employeeId")
	private Employee employeeId;

	@Lob
	private String message;

	//bi-directional many-to-one association to ProspectiveInsured
	@ManyToOne
	@JoinColumn(name="prospectiveId")
	private ProspectiveInsured prospectiveInsured;
	
	public InsuredState() {
	}

	public int getInsuredStateId() {
		return this.insuredStateId;
	}

	public void setInsuredStateId(int insuredStateId) {
		this.insuredStateId = insuredStateId;
	}

	public String getCurrentState() {
		return this.currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getDateChanged() {
		return this.dateChanged;
	}

	public void setDateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
	}

	public Employee getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(Employee employeeId) {
		this.employeeId = employeeId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public ProspectiveInsured getProspectiveInsured() {
		return this.prospectiveInsured;
	}
	
	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}
	
	public String formatDateString(){
		if(this.dateChanged != null){
			return PlatformUtils.formatDateToString(new DateTime(Long.parseLong(this.dateChanged)), "MMMM dd, YYYY HH:MM:SS").toString();
		}else{
			return null;
		}
	}
}