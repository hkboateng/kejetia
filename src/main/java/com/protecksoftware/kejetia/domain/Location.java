package com.protecksoftware.kejetia.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Latitude;
import org.hibernate.search.annotations.Longitude;
import org.hibernate.search.annotations.Spatial;
import org.hibernate.search.annotations.SpatialMode;
import org.hibernate.search.annotations.Spatials;
import org.hibernate.search.spatial.Coordinates;


/**
 * The persistent class for the location database table.
 * 
 */
@Spatial
@Indexed
@Entity
@Table(name="location")
@NamedQuery(name="Locations.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable, Coordinates {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int locationId;

	private String geospatial;
	
	@Latitude(of="location")
	private Double latitude;
	
	@Latitude(of="location")
	private Double longitude;	
	
	public Location() {
	}

	public int getLocationId() {
		return this.locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getGeospatial() {
		return this.geospatial;
	}

	public void setGeospatial(String geospatial) {
		this.geospatial = geospatial;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	

	  @Spatial(spatialMode = SpatialMode.HASH)
	  public Coordinates getLocation() {
	    return new Coordinates() {
	      @Override
	      public Double getLatitude() {
	        return latitude;
	      }

	      @Override
	      public Double getLongitude() {
	        return longitude;
	      }
	    };
	  }

	  

}