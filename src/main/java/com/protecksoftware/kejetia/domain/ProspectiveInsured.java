package com.protecksoftware.kejetia.domain;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the prospectiveInsured database table.
 * 
 */
@Entity
@Table(name="prospectiveInsured")
@NamedQuery(name="ProspectiveInsured.findAll", query="SELECT p FROM ProspectiveInsured p")
public class ProspectiveInsured implements Serializable{
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int prospectiveId;

	@Lob
	private String comments;

	private String dateEnquired;

	private String firstname;

	private String insuranceType;

	private String lastname;

	private String middlename;

	private String prospectiveUniqueId;
	
	private String currentState;
	
	@OneToOne
	@JoinColumn(name="addressId")
	private Address addresses;

	//bi-directional many-to-one association to Email
	@OneToOne
	@JoinColumn(name="emailId")
	private Email email;

	//bi-directional many-to-one association to Phone
	@OneToOne
	@JoinColumn(name="phoneId")
	private Phone phone;

	/**
	 * Employee is the Sales Rep or Staff 
	 * that saved Prospective Customer 
	 */
	@ManyToOne
	@JoinColumn(name="employeeId")
	private Employee employee;
	
	//bi-directional one-to-one association to AgentProspectiveInsured
	@OneToOne(mappedBy="prospectiveInsured")
	private AgentProspectiveInsured agentProspect;

	//bi-directional many-to-one association to InsuredState
	@OneToMany(mappedBy="prospectiveInsured")
	private List<InsuredState> insuredStates;

	public ProspectiveInsured() {
	}


	public int getProspectiveId() {
		return this.prospectiveId;
	}

	public void setProspectiveId(int prospectiveId) {
		this.prospectiveId = prospectiveId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDateEnquired() {
		return this.dateEnquired;
	}

	public void setDateEnquired(String dateEnquired) {
		this.dateEnquired = dateEnquired;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getInsuranceType() {
		return this.insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getProspectiveUniqueId() {
		return this.prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}


	public Address getAddresses() {
		return this.addresses;
	}

	public void setAddresses(Address addresses) {
		this.addresses = addresses;
	}

	public Address addAddress(Address address) {
		setAddresses(address);
		return address;
	}

	
	public Email getEmail() {
		return this.email;
	}

	public void setEmail(Email emails) {
		this.email = emails;
	}

	public Email addEmail(Email email) {
		setEmail(email);

		return email;
	}

	public Phone getPhone() {
		return this.phone;
	}

	public void setPhones(Phone phones) {
		this.phone = phones;
	}

	public Phone addPhone(Phone phone) {
		setPhones(phone);
		return phone;
	}



	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getFirstname());
		if(getMiddlename() !=null){
			builder.append(" ");
			builder.append(getMiddlename());
		}
		builder.append(" ");
		builder.append(getLastname());		
		return builder.toString();
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void String(String currentState) {
		this.currentState = currentState;
	}

	
	
}
