package com.protecksoftware.kejetia.domain.dto;

import org.hibernate.search.spatial.Coordinates;

public class LocationDTO {
	private int locationId;

	private Coordinates geospatial;

	private Double latitude;

	private Double longitude;

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public Coordinates getGeospatial() {
		return geospatial;
	}

	public void setGeospatial(Coordinates geospatial) {
		this.geospatial = geospatial;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public LocationDTO(){
		
	}
	
}
