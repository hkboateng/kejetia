package com.protecksoftware.kejetia.pipeline.businessobjects;

public class AuthenticationEmployeeRequest{


	//Whether is save a Company Employee or new Company and its admin.
	private String requestType;//Values are employee or company
	
	private int userId;
	
	private String username;
	
	private String hashpassword;
	
	private String[] roles;

	private boolean enabled;
	
	private int[] roleId;
	
	private String emailAddress;
	
	private int employeeId;
	
	private int companyId;
	
	private String address1;
	
	private String address2;

	private String phoneNumber;
	
	private String city;

	private String firstname;

	private String gender;
	
	private String homephone;

	private String lastname;
	
	private String middlename;

	private String region;

	private String zipcode;
	
	private  String companyNumber;
	
	private int positionId;

	private String employeeType;
	
	/**
	 * Can be Business name or Agent full name
	 */
	private String agentName;
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param hashpassword the hashpassword to set
	 */
	public void setHashpassword(String hashpassword) {
		this.hashpassword = hashpassword;
	}
	/**
	 * @return the hashpassword
	 */
	public String getHashpassword() {
		return hashpassword;
	}


	/**
	 * @return the roles
	 */
	public String[] getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public void setRole(String role){
		if(roles == null){
			String[] roleArr = new String[1];
			roleArr[0] = role;
			roles = roleArr;
		}
	}
	/**
	 * @param username
	 * @param hashpassword
	 * @param roles
	 */
	public AuthenticationEmployeeRequest(String username, String hashpassword, String[] roles) {
		this.username = username;
		this.hashpassword = hashpassword;
		this.roles = roles;
	}
	
	public AuthenticationEmployeeRequest() {
		super();
	}
	
	public AuthenticationEmployeeRequest(EmployeeRequest request) {
		this.hashpassword = request.getHashpassword();
		this.username = request.getUsername();
		this.roleId = request.getRoleId();
		this.requestType = request.getRequestType();
		this.region = request.getRegion();
		this.firstname = request.getFirstname();
		this.address1 = request.getAddress1();
		this.address2 = request.getAddress2();
		this.city = request.getCity();
		this.phoneNumber = request.getPhoneNumber();
		this.emailAddress = request.getEmailAddress();
		this.homephone = request.getHomephone();
		this.lastname = request.getLastname();
		this.middlename = request.getMiddlename();
		this.companyId = request.getCompanyId();
		this.positionId = request.getPositionId();
		this.gender = request.getGender();
		this.employeeType = request.getEmployeeType();
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the employeeId
	 */
	public int getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the companyId
	 */
	public int getCompanyId() {
		return companyId;
	}


	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}



	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the homephone
	 */
	public String getHomephone() {
		return homephone;
	}

	/**
	 * @param homephone the homephone to set
	 */
	public void setHomephone(String homephone) {
		this.homephone = homephone;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the middlename
	 */
	public String getMiddlename() {
		return middlename;
	}

	/**
	 * @param middlename the middlename to set
	 */
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @return the companyNumber
	 */
	public  String getCompanyNumber() {
		return companyNumber;
	}

	/**
	 * @param companyNumber the companyNumber to set
	 */
	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int[] getRoleId() {
		return roleId;
	}

	public void setRoleId(int[] roleId) {
		this.roleId = roleId;
	}

	public void setRoleId(int roleId) {
		int[] roleIdArr = new int[1];
		roleIdArr[0] = roleId;
		this.roleId = roleIdArr;
	}

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	
	
}
