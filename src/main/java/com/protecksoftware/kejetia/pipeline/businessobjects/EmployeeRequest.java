package com.protecksoftware.kejetia.pipeline.businessobjects;

import com.protecksoftware.kejetia.domain.Employee;
import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.domain.Location;
import com.protecksoftware.kejetia.domain.Position;

public class EmployeeRequest {
	//Whether is save a Company Employee or new Company and its admin.
	private String requestType;//Values are employee or company
	
	private int userId;
	
	private String username;
	
	private String hashpassword;
	
	private String[] roles;

	private boolean enabled;
	
	private int[] roleId;
	
	private String emailAddress;
	
	private int employeeId;
	
	private int companyId;
	
	private String address1;
	
	private String address2;

	private String phoneNumber;
	
	private String city;

	private String firstname;

	private String gender;
	
	private String homephone;

	private String lastname;
	
	private String middlename;

	private String region;

	private String zipcode;
	
	private  String companyNumber;
	
	private int positionId;
	
	private Location location;

	private String employeeType;
	
	private String[] insuranceType;
	
	/**
	 * Can be Business name or Agent full name
	 */
	private String agentName;
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHashpassword() {
		return hashpassword;
	}

	public void setHashpassword(String hashpassword) {
		this.hashpassword = hashpassword;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int[] getRoleId() {
		return roleId;
	}

	public void setRoleId(int[] roleId) {
		this.roleId = roleId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHomephone() {
		return homephone;
	}

	public void setHomephone(String homephone) {
		this.homephone = homephone;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Employee getEmployeeInstance() {
		Employee employee = new Employee();
		Position position = new Position();
		position.setPositionId(getPositionId());
		employee.setPosition(position);
		employee.setEmployeeType(employeeType);
		return employee;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}
	
	public String getEmployeeFullName() {
		StringBuilder builder = new StringBuilder();
		builder.append(getFirstname());
		if(getMiddlename() !=null){
			builder.append(" ");
			builder.append(getMiddlename());
		}
		builder.append(" ");
		builder.append(getLastname());		
		return builder.toString();
	}

	public String[] getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String[] insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
}
