package com.protecksoftware.kejetia.pipeline.businessobjects;

import java.util.List;

import com.protecksoftware.kejetia.domain.Employee;
import com.protecksoftware.kejetia.domain.InsuranceAgent;

public class EmployeeResponse{

	private int employeeId;
	
	private String responseStatus;
	
	private int responseCode;

	private boolean result;
	
	private String message;
	
	private Employee employee;
	
	private List<Employee> employeeList;
	
	private List<InsuranceAgent> insuranceAgent;
	
	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
		
	}
	
	public List<Employee> getEmployeeList(){
		return employeeList;
	}

	public List<InsuranceAgent> getInsuranceAgent() {
		return insuranceAgent;
	}

	public void setInsuranceAgent(List<InsuranceAgent> insuranceAgent) {
		this.insuranceAgent = insuranceAgent;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	
}
