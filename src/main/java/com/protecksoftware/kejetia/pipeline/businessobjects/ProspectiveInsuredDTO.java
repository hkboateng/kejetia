package com.protecksoftware.kejetia.pipeline.businessobjects;

import java.util.List;

import org.joda.time.DateTime;

import com.protecksoftware.kejetia.domain.Address;
import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.domain.InsuredState;
import com.protecksoftware.kejetia.domain.ProspectiveInsured;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;
import com.protecksoftware.kejetia.pipeline.utils.RegionUtils;

public class ProspectiveInsuredDTO {
	private int prospectiveId;
	
	private String prospectiveUniqueId;
	
	private String emailAddress;

	private int employeeId;

	private String firstname;

	private String insuranceType;
	
	private String lastname;

	private String middlename;
	
	private String fullname;

	private String phoneNumber;

	private String phoneType;
	
	private String dateEnquired;
	
	private String addressLocation;
	
	private String cityLocation;
	
	private String region;
	
	private List<InsuredState> insuredStatusList;
	
	private InsuranceAgentDTO assignedAgent;
	
	private boolean hasAssignedAgent;
	
	private String currentStatus;
	public ProspectiveInsuredDTO(ProspectiveInsured insured) {
		super();
		this.prospectiveId = insured.getProspectiveId();
		this.employeeId = insured.getEmployee().getEmployeeId();
		this.fullname = insured.toString();
		this.firstname = insured.getFirstname();
		this.middlename = insured.getMiddlename();
		this.lastname = insured.getLastname();
		this.insuranceType = insured.getInsuranceType();
		this.phoneNumber = insured.getPhone().getPhoneNumber();
		this.emailAddress = insured.getEmail().getEmailAddress();
		setAddressLocation(insured.getAddresses());
		setDateEnquired(insured.getDateEnquired());
		this.prospectiveUniqueId = insured.getProspectiveUniqueId();
		this.cityLocation = insured.getAddresses().getCity();
		this.region = insured.getAddresses().getRegion();
		this.setCurrentStatus(insured.getCurrentState());
	}

	public List<InsuredState> getInsuredStatusList() {
		return insuredStatusList;
	}

	public void setInsuredStatusList(List<InsuredState> insuredStatusList) {
		this.insuredStatusList = insuredStatusList;
	}

	public int getProspectiveId() {
		return prospectiveId;
	}

	public void setProspectiveId(int prospectiveId) {
		this.prospectiveId = prospectiveId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getDateEnquired() {
		return dateEnquired;
	}

	public void setDateEnquired(String dateEnquired) {
		DateTime date = new DateTime(Long.valueOf(dateEnquired)).toDateTime();
		this.dateEnquired = PlatformUtils.formatDateToString(date, "MMMM, dd YYYY hh:mm:ss a");
	}
	
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getAddressLocation() {
		return addressLocation;
	}

	public void setAddressLocation(String addressLocation) {
		this.addressLocation = addressLocation;
	}
	
	public void setAddressLocation(Address addressLocation) {
		StringBuilder sbr = new StringBuilder();
		sbr.append(addressLocation.getCity()).append(", ").append(RegionUtils.getRegionName(addressLocation.getRegion()));
		this.addressLocation = sbr.toString();
	}

	public String getProspectiveUniqueId() {
		return prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}

	public String getCityLocation() {
		return cityLocation;
	}

	public void setCityLocation(String cityLocation) {
		this.cityLocation = cityLocation;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public InsuranceAgentDTO getAssignedAgent() {
		return assignedAgent;
	}

	public void setAssignedAgent(InsuranceAgent assignedAgent) {
		this.assignedAgent = new InsuranceAgentDTO(assignedAgent);
		if(this.assignedAgent != null){
			setHasAssignedAgent(true);
		}else{
			setHasAssignedAgent(false);
		}
	}

	public boolean isHasAssignedAgent() {
		return hasAssignedAgent;
	}

	public void setHasAssignedAgent(boolean hasAssignedAgent) {
		this.hasAssignedAgent = hasAssignedAgent;
	}

	public String getCurrentStatus() {
		
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	
}
