package com.protecksoftware.kejetia.pipeline.businessobjects;

import java.util.List;

import com.protecksoftware.kejetia.domain.ProspectiveInsured;

public class ProspectiveResponse extends KejetiaResponse{

	private ProspectiveInsured insured;
	
	private List<ProspectiveInsured> prospectiveInsuredList;

	public ProspectiveInsured getInsured() {
		return insured;
	}

	public void setInsured(ProspectiveInsured insured) {
		this.insured = insured;
	}

	public List<ProspectiveInsured> getProspectiveInsuredList() {
		return prospectiveInsuredList;
	}

	public void setProspectiveInsuredList(List<ProspectiveInsured> prospectiveInsuredList) {
		this.prospectiveInsuredList = prospectiveInsuredList;
	}
	
	
}
