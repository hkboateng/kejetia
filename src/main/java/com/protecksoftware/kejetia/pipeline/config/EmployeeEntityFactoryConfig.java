package com.protecksoftware.kejetia.pipeline.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class EmployeeEntityFactoryConfig {
	
	private static final String packagesToScan = "com.protecksoftware.kejetia";
	
	@Autowired
	private DataSource dataSource;
	
	  @Bean
	  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
	    LocalContainerEntityManagerFactoryBean entityManagerFactory =
	        new LocalContainerEntityManagerFactoryBean();
	    
	    entityManagerFactory.setDataSource(dataSource);
	    entityManagerFactory.setPackagesToScan(EmployeeEntityFactoryConfig.packagesToScan);
	    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	    vendorAdapter.setShowSql(false);
	    
	    vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
	    entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
	    
	    return entityManagerFactory;
	  }

	  /**
	   * Declare the transaction manager.
	   */
	 @Bean
	 public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
	    transactionManager.setDataSource(dataSource);
	    transactionManager.setSessionFactory(sessionFactory().getObject());
	    
	    return transactionManager;
	}
	 
	 @Bean

	 public HibernateJpaSessionFactoryBean sessionFactory() {
	     return new HibernateJpaSessionFactoryBean();
	 }
}
