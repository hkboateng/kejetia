package com.protecksoftware.kejetia.pipeline.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@RabbitListener(queues = {"employeeRegistrationQueue","authenticationNotificationQueue","changePasswordQueue","forgotPasswordQueue","forgotUsernameQueue"})
//@RabbitListener(queues = "employeeRegistrationQueue")
@Configuration
public class MessengerConfig {
	
	@Bean
	public Queue employeeRegistrationQueue() {
		return new Queue("employeeRegistrationQueue",true);
	}
	
	
}
