package com.protecksoftware.kejetia.pipeline.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.protecksoftware.kejetia.resource.api.EmployeeResource;

@Component
public class RestServiceConfig extends ResourceConfig {

	private RestServiceConfig() {
		register(EmployeeResource.class);
	}

}
