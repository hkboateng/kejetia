package com.protecksoftware.kejetia.pipeline.domain;

public class InsuranceAgent extends Employee implements Staff{
	private int agentId;
	
	private String agentName;

	private String agentNumber;

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	@Override
	public String getStaffType() {
		// TODO Auto-generated method stub
		return StaffCategory.valueOf(("insuranceAgent")).getStaffCategory();
	}
	
	
}
