package com.protecksoftware.kejetia.pipeline.domain;

public enum StaffCategory {
	INSURANCE_AGENT("insuranceAgent"),
	SALES_PERSON("salesperson");
	
	StaffCategory(String staff){
		this.staff = staff;
	}
	private String staff;
	
	public String getStaffCategory(){
		return this.staff;
	}
}
