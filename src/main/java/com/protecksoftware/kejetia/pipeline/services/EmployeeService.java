package com.protecksoftware.kejetia.pipeline.services;

import org.springframework.stereotype.Component;

import com.protecksoftware.kejetia.application.request.AuthenticationEmployeeRequest;
import com.protecksoftware.kejetia.domain.Employee;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeDTO;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeRequest;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeResponse;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

@Component
public interface EmployeeService extends KejetiaService<Employee,EmployeeResponse,EmployeeRequest> {
	
	/**
	 * Eg. Find all Insurance Agents, Direct Sales reps
	 * @param uniqueId -
	 * @param searchType - employeeId, employeeNumber, position
	 * @return EmployeeResponse
	 */
	EmployeeResponse findAllEmployeeByPosition(String searchType, String uniqueId);
	
	EmployeeDTO findByEmployeeId(int Id);

	EmployeeDTO saveEmployeeDetails(Employee employee);
	
	EmployeeDTO saveEmployeeDetails(Employee employee,String employeeType);

	EmployeeResponse findAllEmployeeByPosition(String position);

	EmployeeResponse save(AuthenticationEmployeeRequest employeeRequest);

	EmployeeResponse getAllInsuranceAgent(String position) throws PlatformException;

	EmployeeResponse getAllEmployeeByPosition(String position) throws PlatformException;

	EmployeeDTO saveEmployeeDetails(EmployeeRequest employeeReqest);

	boolean deleteEmployee(int employeeId);
}
