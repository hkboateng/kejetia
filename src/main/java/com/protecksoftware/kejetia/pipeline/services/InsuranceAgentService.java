package com.protecksoftware.kejetia.pipeline.services;

import java.util.List;

import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

public interface InsuranceAgentService {

	List<InsuranceAgentDTO> findAgentByLocation(String location) throws PlatformException;

	InsuranceAgent findInsuranceAgentById(int agentId);

	InsuranceAgent findInsuranceAgentByEmployeeId(Integer employeeId);
}
