package com.protecksoftware.kejetia.pipeline.services;

import java.util.List;

import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

public interface KejetiaService<E,R,Q> {
	List<E> getAll() throws PlatformException;
	
	R findById(int Id);
	
	R create(Q e);
	
	List<R> search(String... str);
}
