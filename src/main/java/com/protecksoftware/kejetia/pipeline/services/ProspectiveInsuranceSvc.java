package com.protecksoftware.kejetia.pipeline.services;

import java.util.List;

import org.hibernate.Session;

import com.protecksoftware.kejetia.application.request.AgentProspectiveRequest;
import com.protecksoftware.kejetia.domain.Address;
import com.protecksoftware.kejetia.domain.AgentProspectiveInsured;
import com.protecksoftware.kejetia.domain.Email;
import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.domain.InsuredState;
import com.protecksoftware.kejetia.domain.Phone;
import com.protecksoftware.kejetia.domain.ProspectiveInsured;
import com.protecksoftware.kejetia.pipeline.businessobjects.ProspectiveInsuredDTO;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;


public interface ProspectiveInsuranceSvc {
	
	ProspectiveInsured saveProspectiveCustomer(ProspectiveInsured prospec);
	
	ProspectiveInsured getProspectiveCustomer(int prospectiveId);
	
	ProspectiveInsuredDTO getProspectiveCustomer(String prospectiveUniqueId,boolean addInsuredStateNote);
	
	List<ProspectiveInsuredDTO> getProspectiveInsuredByCity(String city);

	AgentProspectiveInsured assignProspectToAgent(AgentProspectiveRequest agentProspectiveRequest) throws PlatformException;

	List<ProspectiveInsuredDTO> getProspectiveInsuredByEmployeeId(Integer employeeId) throws PlatformException;

	ProspectiveInsured saveProspectiveCustomerDetails(ProspectiveInsured prospect, InsuredState insuredState,
			Address address, Email emailAddress, Phone phoneNumber) throws PlatformException ;

	Email saveEmail(Email email, Session session);

	Phone savePhone(Phone phone, Session session);

	Address saveAddress(Address address, Session session);

	InsuredState saveInsuredState(InsuredState insuredState, Session session);

	//SalesRep findSalesRepByEmployeeId(Integer employeeId);

	InsuranceAgent findAgentAssignedToProspectiveCustomer(Integer prospectiveId) throws PlatformException;

	List<ProspectiveInsuredDTO> getProspectiveInsuredByEmployeeId(Integer employeeId, String currentState)
			throws PlatformException;

	List<ProspectiveInsuredDTO> findProspectiveCustomerForAgent(Integer employeeId, boolean onlyNew);


}
