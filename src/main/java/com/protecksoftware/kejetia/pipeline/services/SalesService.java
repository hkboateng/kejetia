package com.protecksoftware.kejetia.pipeline.services;

import java.util.List;

import com.protecksoftware.kejetia.domain.AgentProspectiveInsured;
import com.protecksoftware.kejetia.domain.ProspectiveInsured;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.domain.dto.LocationDTO;
import com.protecksoftware.kejetia.pipeline.businessobjects.ProspectiveRequest;
import com.protecksoftware.kejetia.pipeline.businessobjects.ProspectiveResponse;

public interface SalesService  extends KejetiaService<ProspectiveInsured,ProspectiveResponse,ProspectiveRequest> {

	List<InsuranceAgentDTO>  findAgentByLocation(LocationDTO location);

	AgentProspectiveInsured addAgentProspectiveAssignment(AgentProspectiveInsured agentProspectiveInsured);
}
