package com.protecksoftware.kejetia.pipeline.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.protecksoftware.kejetia.domain.Address;
import com.protecksoftware.kejetia.domain.Employee;
import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.pipeline.businessobjects.AgentDTO;
import com.protecksoftware.kejetia.pipeline.businessobjects.AuthenticationEmployeeRequest;
import com.protecksoftware.kejetia.pipeline.businessobjects.AuthenticationResponse;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeDTO;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeRequest;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeResponse;
import com.protecksoftware.kejetia.pipeline.services.EmployeeService;
import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class.getName());

	@Resource(name="sessionFactory")
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getAll() throws PlatformException{
		List<Employee> employeeList = null;
		Session session = null;
		try{
			session = sessionFactory.openSession();
			employeeList = session.createQuery("From Employee")
					.setCacheable(true)
					.list();			
		}catch(HibernateException e){
			logger.error(e.getMessage(), e);
			throw new PlatformException(e);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw new PlatformException(e);
		}finally{
			if(session != null){
				session.close();
			}
		}

		return employeeList;
	}

	@Override
	public EmployeeResponse findById(int Id) {
		EmployeeResponse response = new EmployeeResponse();
		Session session = null;
		try{
			session = sessionFactory.openSession();
			Employee employee = session.get(Employee.class, Id);
			if(employee == null){
				response.setResult(false);
				response.setMessage("Employee Search was successful.");			
			}else{
				response.setEmployee(employee);
				response.setResult(true);
				response.setMessage("Employee Search was successful.");
			}			
		}catch(HibernateException e){
			logger.error(e.getMessage(), e);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}finally{
			if(session != null){
				session.close();
			}
		}


		return response;
	}
	
	public EmployeeResponse generateEmployeeResponse(AuthenticationResponse authenticationResponse) {
		EmployeeResponse response = new EmployeeResponse();
		if(authenticationResponse == null){
			response.setResult(false);
			response.setMessage("Employee Search was successful.");	
			
		}else{
			response.setEmployee(authenticationResponse.getEmployee());
			response.setResult(true);
			response.setMessage("Employee Search was successful.");
			response.setEmployeeId(authenticationResponse.getEmployeeId());
		}
		return response;
	}
	
	@Override
	public EmployeeResponse create(EmployeeRequest employeeRequest) {
		Client client = PlatformUtils.getRESTClientInstance();
		AuthenticationResponse authenticationResponse = null;
		EmployeeResponse employeeResponse = null;
		EmployeeDTO employeeDTO = null;
		try {
			AuthenticationEmployeeRequest authRequest = new AuthenticationEmployeeRequest(employeeRequest);
			authenticationResponse = saveEmployeeInformation(authRequest);
			employeeRequest.setEmployeeId(authenticationResponse.getEmployeeId());
			employeeResponse = generateEmployeeResponse(authenticationResponse);
			employeeResponse.setEmployeeId((int) authenticationResponse.getEmployeeId());
			/**
			String entity = PlatformUtils.convertToJSON(authRequest);
	
			Response response = client.target(saveEmployee)
					.request()
					.post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
			if(response.getStatus() == 200){
					String strResponse =  response.readEntity(String.class);
					authenticationResponse = PlatformUtils.convertFromJSON(AuthenticationResponse.class, strResponse);
					
			}**/
			if(employeeResponse != null){
				employeeDTO =saveEmployeeDetails(employeeRequest);	
				if(employeeDTO != null){
					Employee empl = findById(employeeResponse.getEmployeeId()).getEmployee();
					employeeResponse.setEmployee(empl);
					employeeResponse.setResult(true);
				}else{
					employeeResponse.setResult(false);
				}
				
			}			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}	


		return employeeResponse;
	}

	@Override
	public boolean deleteEmployee(int employeeId){
		boolean status = false;
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("delete from employee where employeeId =?");
			ps.setInt(1, employeeId);
			ps.executeUpdate();
			status = true;
		}catch(SQLException e){
			
		}finally{
			closeDB(conn,ps,null);
		}
		return status;
	}
	@Override
	public EmployeeDTO saveEmployeeDetails(EmployeeRequest employeeReqest) {
		EmployeeDTO employeeDTO = new EmployeeDTO();
		AgentDTO agentDTO = null;
		
		String sql = null;
		String employeeNumber = null;
		String employeeType = employeeReqest.getEmployeeType();
		if(employeeType.equalsIgnoreCase("salesRep")){
			sql = "insert into salesRep (salesRepId,salesRepNumber,employeeId) values (NULL,?,?)";
			employeeNumber = PlatformUtils.generateSalesRepNumber();
		}else if(employeeType.equalsIgnoreCase("insuranceAgent")){
			sql = "insert into insuranceAgent (agentId,agentNumber,agentName,employeeId,locationId,agentAddressId) values (NULL,?,?,?,?,?)";
			employeeNumber = PlatformUtils.generateEmployeeNumber();
		}else{
			employeeNumber = PlatformUtils.generateEmployeeNumber();
		}
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			int key = 0;
			if(employeeType.equalsIgnoreCase("insuranceAgent")){
				agentDTO = new AgentDTO();
				ps = conn.prepareStatement("insert into location (locationId,longitude,latitude,geospatial) values (null,?,?,?)",Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, employeeReqest.getLocation().getLongitude().toString());
				ps.setString(2, employeeReqest.getLocation().getLatitude().toString());
				ps.setString(3, employeeReqest.getLocation().getGeospatial());
				int keyId = ps.executeUpdate();	
				conn.setSavepoint("saveInsuranceAgentLocation");
				rs = ps.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(keyId);
					agentDTO.setLocationId(key);
				}
				
				ps.close();
				Address agentAddress = saveAgentAddress(conn,employeeReqest);
				ps = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, employeeNumber);
				ps.setString(2, employeeReqest.getAgentName());
				ps.setInt(3, employeeReqest.getEmployeeId());
				ps.setInt(4, key);
				ps.setInt(5, agentAddress.getAddressId());
				keyId = ps.executeUpdate();
				rs = ps.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(keyId);
					agentDTO.setAgentId(key);
				}
				conn.setSavepoint("saveInsuranceAgent");
				saveAgentInsuranceTypeDetail(conn,employeeReqest,key);
				employeeDTO.setAgentDTO(agentDTO);
			}else{
				/*** Sales Rep - Saving into Database ***/
				ps = conn.prepareStatement(sql);
				ps.setString(1, employeeNumber);
				ps.setInt(2, employeeReqest.getEmployeeId());	
				ps.execute();
				
			}
			conn.commit();
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			rollback(conn);
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			rollback(conn);
		}finally{
			closeDB(conn,ps,rs);
		}
		
		return employeeDTO;
	}
	@Override
	public List<EmployeeResponse> search(String... str) {
		// TODO Auto-generated method stub
		return null;
	}

	//TODO - Change Employee from being a class to Interface
	@SuppressWarnings("unchecked")
	@Override
	public EmployeeResponse getAllEmployeeByPosition(String position) throws PlatformException {
		List<Employee> employeeList = null;
		EmployeeResponse response = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			if(position != null && position.equalsIgnoreCase("insuranceAgent")){
				employeeList = session.createQuery("From InsuranceAgent")
						.setCacheable(true)
						.list();
			}else if(position != null && position.equalsIgnoreCase("salesperson")){
				employeeList = session.createQuery("From salesRep")
						.setCacheable(true)
						.list();
			}else{
			employeeList = session.createQuery("From Employee")
					.setCacheable(true)
					.list();	
			}
		}catch(HibernateException e){
			logger.error(e.getMessage(), e);
			throw new PlatformException(e);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw new PlatformException(e);
		}finally{
			if(session != null){
				session.close();
			}
			if(employeeList != null){
				response = new EmployeeResponse();
				response.setEmployeeList(employeeList);
				response.setResult(true);
			}else{
				response = new EmployeeResponse();
				response.setResult(true);
				response.setMessage("No Employee was found for your search :-"+position);
			}
			
		}

		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EmployeeResponse getAllInsuranceAgent(String position) throws PlatformException {
		List<InsuranceAgent> employeeList = null;
		EmployeeResponse response = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			if(position != null && position.equalsIgnoreCase("insuranceAgent")){
				employeeList = session.createQuery("From InsuranceAgent")
						.setCacheable(true)
						.list();
			}
		}catch(HibernateException e){
			logger.error(e.getMessage(), e);
			throw new PlatformException(e);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw new PlatformException(e);
		}finally{
			if(session != null){
				session.close();
			}
			if(employeeList != null){
				response = new EmployeeResponse();
				response.setInsuranceAgent(employeeList);
				response.setResult(true);
			}else{
				response = new EmployeeResponse();
				response.setResult(true);
				response.setMessage("No Employee was found for your search :-"+position);
			}
		}

		return response;
	}
	
	public Address saveAgentAddress(Connection conn,EmployeeRequest employeeRequest){
		Address agentAddress = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int key = 0;
		try{
			if(conn.isClosed()){
				conn = dataSource.getConnection();
			}
			ps = conn.prepareStatement("insert into address (addressId,address1,address2,city,region,country,addressType) values (NULL,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, employeeRequest.getAddress1());
			ps.setString(2, employeeRequest.getAddress2());
			ps.setString(3, employeeRequest.getCity());
			ps.setString(4, employeeRequest.getRegion());
			ps.setString(5, "Ghana");
			ps.setString(6, "Agent Address");
			int keyId = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getInt(keyId);
				agentAddress = new Address();
				agentAddress.setAddressId(key);
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			rollback(conn);
		}catch(Exception e){
			rollback(conn);
			logger.error(e.getMessage(),e);
		}finally{
			closeDB(null,ps,rs);
		}
		return agentAddress;
	}

	public void saveAgentInsuranceTypeDetail(Connection conn,EmployeeRequest employeeRequest,int agentId){

		PreparedStatement ps = null;

		try{
			if(conn.isClosed()){
				conn = dataSource.getConnection();
			}
			ps = conn.prepareStatement("insert into agentInsuranceDetails (agentInsuranceDetailsId,agentId,insuranceType,dateCreated) values (NULL,?,?,NULL)",Statement.RETURN_GENERATED_KEYS);
			for(int i=0; i < employeeRequest.getInsuranceType().length;i++){
				ps.setInt(1, agentId);
				ps.setString(2,employeeRequest.getInsuranceType()[i]);
				ps.executeUpdate();
			}

		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			rollback(conn);
		}catch(Exception e){
			rollback(conn);
			logger.error(e.getMessage(),e);
		}finally{
			closeDB(null,ps,null);
		}
	}
	
	private void rollback(Connection conn) {
		try {
			conn.rollback();
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
		
	}
	
	private void closeDB(Connection conn, PreparedStatement ps,ResultSet rs){
		try {
			if(conn != null){
				conn.close();
			}
			if(ps != null){
				ps.close();
			}
			if(rs != null){
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
	}
	
	public AuthenticationResponse saveEmployeeInformation(AuthenticationEmployeeRequest request) {
		logger.info("Started the process of saving Employee information for Company Id: "+request.getCompanyNumber());
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationResponse response = null;
		int key = 0;
		try{
			int employeeNumber = PlatformUtils.getEmployeeNumber();
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into employee (employeeId,address1,address2,phoneNumber,city,emailAddress,employeeNumber,firstname,gender,lastname,middlename,region,companyId,positionId,employeeType) values (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,request.getAddress1());
			ps.setString(2, request.getAddress2());
			ps.setString(3,request.getPhoneNumber());
			ps.setString(4,request.getCity());
			ps.setString(5,request.getEmailAddress());
			ps.setString(7,request.getFirstname());
			ps.setLong(6,employeeNumber);
			ps.setString(8,request.getGender());
			ps.setString(9,request.getLastname());
			ps.setString(10,request.getMiddlename());
			ps.setString(11,request.getRegion());
			ps.setLong(12,request.getCompanyId());
			ps.setLong(13,request.getPositionId());
			ps.setString(14, request.getEmployeeType());
			int Id = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getInt(Id);
				response = new AuthenticationResponse();
				response.setResult(true);
				response.setEmployeeId(key);
				response.setMessage("Employee has being saved successfully. New Employee Id:"+key);
				request.setEmployeeId(key);
				if(request.getRequestType().equalsIgnoreCase("employee")){
					//employeeResponse = addTempEmployeeAccount(request,url,conn);
					//String responseString =  AuthenticationUtils.convertObjectToJSON(employeeResponse);
					//employeeRegistrationTemplate.convertAndSend(responseString);		
				}
			}

			logger.info("Employee with Id: "+key+" has being saved successfully for company with Id: "+request.getCompanyNumber());
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return response;
	}
	/**
	 *   public List<User> search(String text) {
    
    // get the full text entity manager
    FullTextEntityManager fullTextEntityManager =
      org.hibernate.search.jpa.Search.
      getFullTextEntityManager(entityManager);
    
    // create the query using Hibernate Search query DSL
    QueryBuilder queryBuilder = 
      fullTextEntityManager.getSearchFactory()
      .buildQueryBuilder().forEntity(User.class).get();
    
    // a very basic query by keywords
    org.apache.lucene.search.Query query =
      queryBuilder
        .keyword()
        .onFields("name", "city", "email")
        .matching(text)
        .createQuery();

    // wrap Lucene query in an Hibernate Query object
    org.hibernate.search.jpa.FullTextQuery jpaQuery =
      fullTextEntityManager.createFullTextQuery(query, User.class);
  
    // execute search and return results (sorted by relevance as default)
    @SuppressWarnings("unchecked")
    List<User> results = jpaQuery.getResultList();
    
    return results;
} 
	 */

	@Override
	public EmployeeResponse findAllEmployeeByPosition(String searchType, String uniqueId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeDTO findByEmployeeId(int Id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeDTO saveEmployeeDetails(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeDTO saveEmployeeDetails(Employee employee, String employeeType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeResponse findAllEmployeeByPosition(String position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeResponse save(
			com.protecksoftware.kejetia.application.request.AuthenticationEmployeeRequest employeeRequest) {
		// TODO Auto-generated method stub
		return null;
	}


}
