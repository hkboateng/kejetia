package com.protecksoftware.kejetia.pipeline.services.impl;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.pipeline.services.InsuranceAgentService;
import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

@Transactional
public class InsuranceAgentServiceImpl implements InsuranceAgentService {
	private static final Logger logger = LoggerFactory.getLogger(InsuranceAgentServiceImpl.class.getSimpleName());
	
	private @Value("${waitTimeNumber}") String waitTimeNumber;
	
	private @Value("${findAgentByLocationUrl}") String findAgentByLocation;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<InsuranceAgentDTO> findAgentByLocation(String location) throws PlatformException {
		List<InsuranceAgentDTO> agentList = null;
		
		try{
		//	String entity = PlatformUtils.(location);
			String responseString = null;
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(findAgentByLocation)
					.queryParam("location", location)
					.request(MediaType.APPLICATION_JSON)
					.get();
			if(response.getStatus() == 200){
				responseString = response.readEntity(String.class);
				agentList = PlatformUtils.convertFromJson(new TypeReference<List<InsuranceAgentDTO>> (){}, responseString);
				
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return agentList;
	}
	
	/**
	 * Find an Insurance Agent by Id
	 * @param agentId
	 * @return
	 */
	@Override
	public InsuranceAgent findInsuranceAgentById(int agentId){
		InsuranceAgent agent = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			agent = session.get(InsuranceAgent.class, agentId);
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}
		
		return agent;
	}

	@Override
	public InsuranceAgent findInsuranceAgentByEmployeeId(Integer employeeId) {
		Session session = null;
		InsuranceAgent agent = null;
		try{
			session = getSessionFactory().openSession();
			agent = (InsuranceAgent) session.createQuery("from InsuranceAgent i where i.employee.employeeId =:employeeId")
			.setParameter("employeeId", employeeId)
			.uniqueResult();

		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}
		return agent;
	}

}
