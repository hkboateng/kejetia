package com.protecksoftware.kejetia.pipeline.services.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Statement;
import com.protecksoftware.kejetia.application.request.AgentProspectiveRequest;
import com.protecksoftware.kejetia.domain.Address;
import com.protecksoftware.kejetia.domain.AgentProspectiveInsured;
import com.protecksoftware.kejetia.domain.Email;
import com.protecksoftware.kejetia.domain.InsuranceAgent;
import com.protecksoftware.kejetia.domain.InsuredState;
import com.protecksoftware.kejetia.domain.Phone;
import com.protecksoftware.kejetia.domain.ProspectiveInsured;
import com.protecksoftware.kejetia.pipeline.businessobjects.ProspectiveInsuredDTO;
import com.protecksoftware.kejetia.pipeline.services.InsuranceAgentService;
import com.protecksoftware.kejetia.pipeline.services.ProspectiveInsuranceSvc;
import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

public class ProspectiveInsuredSvcImpl implements ProspectiveInsuranceSvc{
	private final Logger logger = LoggerFactory.getLogger(ProspectiveInsuredSvcImpl.class.getName());

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private InsuranceAgentService insuranceAgentServiceImpl;
	
	@Override
	@Transactional
	public ProspectiveInsured saveProspectiveCustomer(ProspectiveInsured prospect) {
		try{
			Session session = getSessionFactory().openSession();
			Serializable i = session.save(prospect);
			if( i != null){
				prospect = session.get(ProspectiveInsured.class, i);
			}
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return prospect;
	}

	@Transactional
	@Override
	public InsuredState saveInsuredState(InsuredState insuredState,Session session){
		Connection conn = null;
		PreparedStatement ps = null;
		int key = 0;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into insuredState (insuredStateId,message,employeeId,currentState,dateChanged,prospectiveId) values (NULL,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, insuredState.getMessage());
			ps.setInt(2, insuredState.getEmployeeId().getEmployeeId());
			ps.setString(3, insuredState.getCurrentState());
			ps.setString(4, insuredState.getDateChanged());
			ps.setInt(5, insuredState.getProspectiveInsured().getProspectiveId());
			int keyId = ps.executeUpdate();
			if(keyId > 0){
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next()){key = rs.getInt(keyId);insuredState.setInsuredStateId(rs.getInt(keyId));}
				
			}else{
				
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			closeDB(conn,ps,null);
		}
		return insuredState;		
	}
	
	private void closeDB(Connection conn, PreparedStatement ps,ResultSet rs){
		try {
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
			if(ps != null){
				ps.close();
			}
			if(rs != null){
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
	}
	@Transactional
	@Override
	public Address saveAddress(Address address,Session session){
		int key = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into address (addressId,address1,address2,city,region,addressType,country) values (NULL,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, address.getAddress1());
			ps.setString(2, address.getAddress2());
			ps.setString(3, address.getCity());
			ps.setString(4, address.getRegion());
			ps.setString(5, address.getAddressType());
			ps.setString(6, address.getCountry());
			int keyId = ps.executeUpdate();
			if(keyId > 0){
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(keyId);
					address.setAddressId(rs.getInt(keyId));}
				
			}else{
				
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			closeDB(conn,ps,null);
		}
		return address;			
	}
	
	@Transactional
	@Override
	public Phone savePhone(Phone phone,Session session){
		int key = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into phone (phoneId,phoneNumber,countryCode,phoneType,primaryPhone) values (NULL,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, phone.getPhoneNumber());
			ps.setString(2, phone.getCountrycode());
			ps.setString(3, phone.getPhoneType());
			ps.setBoolean(4,phone.getPrimaryPhone());
			int keyId = ps.executeUpdate();
			if(keyId > 0){
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(keyId);
					phone.setPhoneId(rs.getInt(keyId));}
				
			}else{
				
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			closeDB(conn,ps,null);
		}
		return phone;			
	}

	@Transactional
	@Override
	public Email saveEmail(Email email,Session session){
		int key = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into email (emailId,emailAddress,emailType) values (NULL,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, email.getEmailAddress());
			ps.setString(2, email.getEmailType());
			int keyId = ps.executeUpdate();
			if(keyId > 0){
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next()){key = rs.getInt(keyId);email.setEmailId(rs.getInt(keyId));}
				
			}else{
				
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			closeDB(conn,ps,null);
		}
		return email;			
	}
	
	@Override
	public AgentProspectiveInsured assignProspectToAgent(AgentProspectiveRequest prospect) throws PlatformException{
		AgentProspectiveInsured agentProspect = null;
		int key = 0;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into agentProspective (agentProspectId,agentId,prospectiveId,dateCreated) values (null,?,?,null)",Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, prospect.getAgentId());
			ps.setInt(2, prospect.getProspectiveId());
			
			int keyId = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getInt(keyId);
				agentProspect = new AgentProspectiveInsured();
				agentProspect.setAgentProspectId(key);
				InsuranceAgent agent = insuranceAgentServiceImpl.findInsuranceAgentById(prospect.getAgentId());
				if(agent == null){
					throw new PlatformException("Agent Id: "+prospect.getAgentId()+" cannot be found...");
				}
				boolean assigned = updateProspectiveInsuredStatus(conn,prospect.getProspectiveId(),"assigned",agent.getEmployee().getEmployeeId(),"Assigned to an Insurance Agent");
				if(assigned){
					ProspectiveInsured prospective = getProspectiveCustomer(prospect.getProspectiveId());
					agentProspect.setProspectiveInsured(prospective);
					logger.info("Prospective Insured [with Id: "+ prospect.getProspectiveId()+"] status has being change from Initiated to Assigned.");
					logger.info("Prospective Insured [with Id: "+ prospect.getProspectiveId()+"] has being assigned to agent with Id: ["+ prospect.getAgentId()+"].");
				}else{
					logger.error("Rolling back assigning Prospective Insured [with Id: "+ prospect.getProspectiveId()+"] to Insurance Agent [with Id "+ prospect.getAgentId()+"] becuase there was an error updating Prospective Insured state.");
					rollback(conn);
				}
				
			}
			conn.commit();
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			rollback(conn);
			throw new PlatformException(e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			rollback(conn);
			throw new PlatformException(e);
		}finally{
			closeDB(conn,ps,null);
		}	
		return agentProspect;
	}
	
	private void rollback(Connection conn) {
		try {
			conn.rollback();
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
		
	}
	private boolean updateProspectiveInsuredStatus(Connection conn, int prospectiveId, String status,int agentId,String message) throws PlatformException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean statusChanged = false;
		try{
			if(conn == null || conn.isClosed()){
				conn = dataSource.getConnection();
			}
			ps = conn.prepareStatement("insert into insuredState (insuredStateId,message,employeeId,currentState,dateChanged,prospectiveId) values (null,?,?,?,?,?)");
			ps.setString(1, message);
			ps.setInt(2, agentId);
			ps.setString(3, status);
			ps.setString(4, String.valueOf(new Date().getTime()));
			ps.setInt(5, prospectiveId);
			ps.executeUpdate();
			ps.close();
			
			ps = conn.prepareStatement("update prospectiveInsured set currentState =? where prospectiveId =?");
			ps.setString(1, status);
			ps.setInt(2, prospectiveId);
			
			int keyId = ps.executeUpdate();
			if(keyId == 1){
				statusChanged = true;
			}else{
				statusChanged = false;
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			throw new PlatformException(e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new PlatformException(e);
		}finally{
			closeDB(null,ps,rs);
		}
		return statusChanged;
	}

	@Override
	@Transactional
	public ProspectiveInsured getProspectiveCustomer(int prospectiveId) {
		ProspectiveInsured prospect = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			prospect = session.get(ProspectiveInsured.class, prospectiveId);
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}

		return prospect;
	}

	/**
	 * Load prospective Insured
	 * @param prospectiveUniqueId - uniqueId
	 * @param addInsuredStateNote - if true load prospect Notes and Insured State, else donot load 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public ProspectiveInsuredDTO getProspectiveCustomer(String prospectiveUniqueId,boolean addInsuredStateNote) {
		ProspectiveInsuredDTO prospect = null;
		InsuranceAgent agent= null;
		Session session =  null;
		try{
			session = getSessionFactory().openSession();
			Optional<ProspectiveInsured> option = session.createQuery("from ProspectiveInsured p where prospectiveUniqueId =:prospectiveUniqueId")
					.setParameter("prospectiveUniqueId", prospectiveUniqueId)
					.setCacheable(true)
					.uniqueResultOptional();
			if(option.isPresent()){
				prospect = new ProspectiveInsuredDTO(option.get());
				if(prospect != null && addInsuredStateNote){
					List<InsuredState> insuredStateList =getProspectInsuredStatus(prospect.getProspectiveId());
					if(insuredStateList != null && !insuredStateList.isEmpty()){
						prospect.setInsuredStatusList(insuredStateList);
					}
				}	
				if(!"initiated".equalsIgnoreCase(prospect.getCurrentStatus())){
					agent = findAgentAssignedToProspectiveCustomer(prospect.getProspectiveId());
					if(agent != null){
						prospect.setHasAssignedAgent(true);
						prospect.setAssignedAgent(agent);
					}					
				}

			}
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}

		return prospect;
	}

	@Override
	public List<ProspectiveInsuredDTO> getProspectiveInsuredByCity(String search) {
		return null;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	private List<InsuredState> getProspectInsuredStatus(Integer prospectiveId){
		List<InsuredState> insuredStateList = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			List<InsuredState> option = session.createQuery("from InsuredState i where i.prospectiveInsured.prospectiveId =:prospectiveId")
					.setParameter("prospectiveId", prospectiveId)
					.setCacheable(true)
					.getResultList(); 
			insuredStateList = (option.size() > 0) ? option: null;
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}

		return insuredStateList;		
	}
	/**
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public ProspectiveInsured saveProspectiveCustomer(ProspectiveInsured prospect) {//insert into login_log(username,status,dateTime,companyId) values (?,?,?,?) "
		try{
			Connection conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement("insert into prospectiveInsured (firstname,middlename,lastname,phoneType,phoneNumber,emailAddress,address1,address2,city,region,prospectiveUniqueId,dateEnquired,employeeId,insuranceType) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1,prospect.getFirstname());
			ps.setString(2, prospect.getMiddlename());
			ps.setString(3, prospect.getLastname());
			ps.setString(4, prospect.getPhoneType());
			ps.setString(5,prospect.getPhoneNumber());
			ps.setString(6, prospect.getEmailAddress());
			ps.setString(7, prospect.getAddress1());
			ps.setString(8, prospect.getAddress2());
			ps.setString(9,prospect.getCity());
			ps.setString(10, prospect.getRegion());
			ps.setString(11, prospect.getProspectiveUniqueId());
			ps.setLong(12, prospect.getDateEnquired());			
			ps.setInt(13,prospect.getEmployeeId());
			ps.setString(14, prospect.getInsuranceType());
			ps.executeUpdate();
			prospect = getProspectiveInsured(prospect.getProspectiveUniqueId());			
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}

		return prospect;
	}

	@Transactional
	@Override
	public ProspectiveInsured getProspectiveInsured(int prospectiveId) {
		ProspectiveInsured prospective = null;
		try{
			Session session = getSessionFactory().openSession();
			prospective = session.get(ProspectiveInsured.class, prospectiveId);
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}		
		return prospective;
	}

	@Transactional
	@Override
	public ProspectiveInsured getProspectiveInsured(String prospectiveUniqueId) {
		ProspectiveInsured prospective = null;
			try{
				Session session = getSessionFactory().openSession();
				prospective = (ProspectiveInsured) session.createQuery("From ProspectiveInsured p where p.prospectiveUniqueId =:prospectiveUniqueId")
				.setParameter("prospectiveUniqueId", prospectiveUniqueId)
				.getSingleResult();
			}catch(HibernateException e){
				logger.error(e.getMessage(),e);
			}catch(Exception e){
				logger.error(e.getMessage(),e);
			}		
		return prospective;
	}

	@Transactional
	@Override
	public List<ProspectiveInsured> getProspectiveInsuredByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
***/
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public List<ProspectiveInsuredDTO> getProspectiveInsuredByEmployeeId(Integer employeeId,String currentState) throws PlatformException {
		List<ProspectiveInsuredDTO> prospectInsuredDTOList = null;
		List<ProspectiveInsured> prospectInsuredList = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ProspectiveInsured> query = builder.createQuery(ProspectiveInsured.class);
			Root<ProspectiveInsured> tr = query.from(ProspectiveInsured.class);
			
			//Get all Predicate for query
			List<Predicate> predicate = new ArrayList<Predicate> ();
				predicate.add(builder.equal(tr.get("employee"), employeeId));
			if(currentState != null){
				predicate.add(builder.equal(tr.get("currentState"),currentState));
			}
			if(!predicate.isEmpty()){
				 Predicate[] pr = new Predicate[predicate.size()];
				 predicate.toArray(pr);
				 query.where(pr);
				 prospectInsuredList = session.createQuery(query).getResultList();
			}

			if(prospectInsuredList != null && !prospectInsuredList.isEmpty()){
				prospectInsuredDTOList = new ArrayList<ProspectiveInsuredDTO>();
				ProspectiveInsuredDTO prospectiveInsuredDTO = null;
				for(ProspectiveInsured prospectInsured:prospectInsuredList){
					prospectiveInsuredDTO = new ProspectiveInsuredDTO(prospectInsured);
					prospectInsuredDTOList.add(prospectiveInsuredDTO);
				}
				
			}
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}
		return prospectInsuredDTOList;
	}

	@Override
	@Transactional
	public ProspectiveInsured saveProspectiveCustomerDetails(ProspectiveInsured prospect, InsuredState insuredState,
			Address address, Email emailAddress, Phone phoneNumber) throws PlatformException {
		Session session = null;
		Connection conn = null;
		PreparedStatement ps =  null;
		try{
			session = getSessionFactory().openSession();
			address = saveAddress(address,session);
			emailAddress = saveEmail(emailAddress,session);
			phoneNumber = savePhone(phoneNumber,session);
			prospect.setDateEnquired(PlatformUtils.getUnixDateTimeNow().toString());
			prospect.addAddress(address);
			prospect.addEmail(emailAddress);
			prospect.addPhone(phoneNumber);
			int key = 0;
			try{
				conn = dataSource.getConnection();
				ps = conn.prepareStatement("insert into prospectiveInsured (firstname,middlename,lastname,phoneId,emailId,addressId,prospectiveUniqueId,dateEnquired,employeeId,insuranceType,comments,currentState) values (?,?,?,?,?,?,?,?,?,?,?,?)");
				ps.setString(1,prospect.getFirstname());
				ps.setString(2, prospect.getMiddlename());
				ps.setString(3, prospect.getLastname());
				ps.setInt(4, prospect.getPhone().getPhoneId());
				ps.setInt(5,prospect.getEmail().getEmailId());
				ps.setInt(6, prospect.getAddresses().getAddressId());
				ps.setString(7, prospect.getProspectiveUniqueId());
				ps.setString(8, prospect.getDateEnquired());			
				ps.setInt(9,prospect.getEmployee().getEmployeeId());
				ps.setString(10, prospect.getInsuranceType());
				ps.setString(11, prospect.getComments());
				ps.setInt(12, 0);
				int keyId= ps.executeUpdate();	
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(keyId);
				}
				prospect.setProspectiveId(key);	
				insuredState.setProspectiveInsured(prospect);
				insuredState = saveInsuredState(insuredState,session);
				//updateProspectiveInsuredStatus(conn,key,"Initiated",prospect.getEmployee().getEmployeeId(),"Initied Prospective Insured");
			}catch(HibernateException e){
				logger.error(e.getMessage(),e);
			}catch(Exception e){
				logger.error(e.getMessage(),e);
			}finally{
				closeDB(conn,ps,null);
			}

			return prospect;

			
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}
		return prospect;
	}

	/**
	@Override
	@Transactional
	public SalesRep findSalesRepByEmployeeId(Integer employeeId){
		SalesRep rep = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			rep = session.get(SalesRep.class, employeeId);
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}
		return rep;
	}
	**/
	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public InsuranceAgent findAgentAssignedToProspectiveCustomer(Integer prospectiveId) throws PlatformException{
		AgentProspectiveInsured agentProspectiveInsured = null;
		Session session = null;
		if(prospectiveId == null || prospectiveId < 1){
			return null;
		}
		try{
			session = getSessionFactory().openSession();
			Optional<AgentProspectiveInsured> optional = session.createQuery("from AgentProspectiveInsured ap where ap.prospectiveInsured.prospectiveId =:prospectiveId")
					.setParameter("prospectiveId", prospectiveId)
					.uniqueResultOptional();
			agentProspectiveInsured = optional.orElse(agentProspectiveInsured);
			if(agentProspectiveInsured == null){
				throw new PlatformException("Salesman could not find any Insurance Agent assigned to Prospective Customer with ID: "+prospectiveId);
			}
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}		
		return agentProspectiveInsured.getAgentId();
	}

	@Override
	public List<ProspectiveInsuredDTO> getProspectiveInsuredByEmployeeId(Integer employeeId) throws PlatformException {
		List<ProspectiveInsuredDTO> prospectiveDTO  = getProspectiveInsuredByEmployeeId(employeeId,null);
		return prospectiveDTO;
	}
	
	@Override
	public List<ProspectiveInsuredDTO> findProspectiveCustomerForAgent(Integer employeeId,boolean onlyNew) {
		Session session = null;
		InsuranceAgent agent = null;
		List<ProspectiveInsuredDTO> prospectInsuredDTOList = null;
		List<ProspectiveInsured> prospectInsuredList = null;
		try{
			session = getSessionFactory().openSession();
			agent = (InsuranceAgent) session.createQuery("select prospectiveInsured from AgentProspectiveInsured a where a.agentId.employee.employeeId =:employeeId")
			.setParameter("employeeId", employeeId)
			.getResultList();

		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			session.close();
		}
		return prospectInsuredDTOList;
	}

}
