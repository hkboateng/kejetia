package com.protecksoftware.kejetia.pipeline.services.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.apache.lucene.search.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.query.dsl.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.protecksoftware.kejetia.domain.Address;
import com.protecksoftware.kejetia.domain.AgentProspectiveInsured;
import com.protecksoftware.kejetia.domain.Location;
import com.protecksoftware.kejetia.domain.ProspectiveInsured;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.domain.dto.LocationDTO;
import com.protecksoftware.kejetia.pipeline.businessobjects.ProspectiveRequest;
import com.protecksoftware.kejetia.pipeline.businessobjects.ProspectiveResponse;
import com.protecksoftware.kejetia.pipeline.services.SalesService;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

@Component
@Transactional
public class SalesServiceImpl implements SalesService{

	private static final Logger logger = LoggerFactory.getLogger(SalesServiceImpl.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private DataSource dataSource;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<ProspectiveInsured> getAll() throws PlatformException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProspectiveResponse findById(int Id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProspectiveResponse create(ProspectiveRequest e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProspectiveResponse> search(String... str) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	@Transactional
	public List<InsuranceAgentDTO> findAgentByLocation(LocationDTO location){
		
		List<InsuranceAgentDTO> agentList = null;
		try{
		Session session = getSessionFactory().openSession();
		FullTextSession textSearch = Search.getFullTextSession(session);
		try {
			textSearch.createIndexer(Location.class).threadsToLoadObjects(3).transactionTimeout(360).startAndWait();
		} catch (InterruptedException e) {
			logger.error(e.getMessage(),e);
		}
		QueryBuilder qbuilder = textSearch.getSearchFactory()
								.buildQueryBuilder()
								.forEntity(Location.class)
								.get();
		Query query = qbuilder.spatial()
						.within(50, Unit.KM)
						.ofCoordinates(location.getGeospatial())
						.createQuery();

		 FullTextQuery textQuery= textSearch.createFullTextQuery(query, Location.class);
		 List<Location> list = textQuery.getResultList();
		if(list != null && !list.isEmpty()){
			agentList= new ArrayList<>();
			
			for(Location locate: list){
				InsuranceAgentDTO  agentDTO = findInsuranceAgentByLocation(locate,session);
				if(agentDTO != null){
					agentList.add(agentDTO);
				}
				

			}
		}
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}
		return agentList;
	}
	
	/**
	 * 	private int agentId;
	
	private String firstname;
	
	private String lastname;
	
	private String fullName;
	
	private String address1;
	
	private String address2;
	
	private String insuranceType;
	
	private String phoneNumber;
	
	private String emailAddress;
	
	private String city;
	a.address1,a.address2,a.city,a.region,
	private String region;
	
	private String agentNumber
	 * @param location
	 * @param session
	 * @return
	 */
	@Transactional
	private InsuranceAgentDTO findInsuranceAgentByLocation(Location location,Session session){
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		InsuranceAgentDTO agent =  null;
		try{
			conn = dataSource.getConnection();
			statement = conn.prepareStatement("select ia.agentId,ia.agentNumber,ia.agentName,ia.agentAddressId  from insuranceAgent ia  where ia.locationId =?");
			statement.setInt(1, location.getLocationId());
			statement.closeOnCompletion();
			rs = statement.executeQuery();
			
			if(rs.next()){
				agent = new InsuranceAgentDTO();
				agent.setAgentId(rs.getInt("agentId"));
				agent.setAgentNumber(rs.getString("agentNumber"));
				agent.setFullName(rs.getString("agentName"));
				int agentAddress = rs.getInt("agentAddressId");
				Address address = getAgentAddress(agentAddress,conn);
				String[] insuranceType = getAgentInsuranceService(agent.getAgentId(),conn);
				if(address != null){
					agent.setAddress1(address.getAddress1());
					agent.setAddress2(address.getAddress2());
					agent.setCity(address.getCity());
					agent.setRegion(address.getRegion());
				}
				agent.setInsuranceType(insuranceType);
			}
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
		}finally{
			try {
				if(conn != null){
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(e.getMessage(),e);
			}
		}
		return agent;
	}

	private String[] getAgentInsuranceService(int agentId, Connection conn) {
		PreparedStatement statement = null;
		ResultSet rs = null;
		String[] insuranceList = null;
		try{
			if(conn == null || conn.isClosed()){
				conn = dataSource.getConnection();
			}
			statement =conn.prepareStatement("select count(*) as size, a.insuranceType from agentInsuranceDetails a where a.agentId =? group by insuranceType");
			statement.setInt(1, agentId);
			rs = statement.executeQuery();
			int size = 0;
			if (rs.last()) {
				size = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
			}
			insuranceList = new String[size];
			int r= 0;
				while(rs.next()){
					insuranceList[r] = rs.getString("insuranceType");
					r++;
					if(r == size){
						break;
					}
				}
			}catch(SQLException e){
				logger.error(e.getMessage(),e);
			}finally{
				try {
					if(!conn.isClosed()){
						conn.close();
					}
				} catch (SQLException e) {
					logger.error(e.getMessage(),e);
				}
			}
		
		return insuranceList;
	}

	private Address getAgentAddress(int agentId, Connection conn) {
		PreparedStatement statement = null;
		ResultSet rs = null;
		Address address = null;
		try{
			if(conn == null || conn.isClosed()){
				conn = dataSource.getConnection();
			}
			statement =conn.prepareStatement("select * from address a where a.addressId =?");
			statement.setInt(1, agentId);
			statement.closeOnCompletion();
			rs = statement.executeQuery();
				if(rs.next()){
					address = new Address();
					address.setAddress1(rs.getString("address1"));
					address.setAddress2(rs.getString("address2"));
					address.setCity(rs.getString("city"));
					address.setRegion(rs.getString("region"));
				}
				
			}catch(SQLException e){
				logger.error(e.getMessage(),e);
			}finally{
				try {
					conn.close();
					
				} catch (SQLException e) {
					logger.error(e.getMessage(),e);
				}
			}
		
		return address;
	}
	

	@Transactional
	@Override
	public AgentProspectiveInsured addAgentProspectiveAssignment(AgentProspectiveInsured agentProspectiveInsured){
		AgentProspectiveInsured agentProspective = null;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			Serializable i = session.save(agentProspectiveInsured);
			agentProspectiveInsured = session.get(AgentProspectiveInsured.class,i);
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}
		return agentProspective;
	}
	
	

}
