/**
 * hkboateng
 */
package com.protecksoftware.kejetia.pipeline.utils;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.hibernate.search.spatial.impl.Point;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.protecksoftware.kejetia.domain.AgentGeoLocation;
import com.protecksoftware.kejetia.domain.Location;
import com.protecksoftware.kejetia.domain.dto.LocationDTO;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

/**
 * @author hkboateng
 * 
 * Utilities that are common through out the application
 *
 */
public class PlatformUtils {

	private PlatformUtils(){}
	
	public static String getProspectiveUniqueId(){

		StringBuilder sbr = new StringBuilder();
		String numbers = RandomStringUtils.randomNumeric(8);
		sbr.append("AY")
			.append("0")
			.append(numbers);
			
		return sbr.toString();	
	}
	public static Long getUnixDateTimeNow(){
		return LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
	}
	public static String formatDateToString(DateTime date,String dateFormat){
		DateTimeFormatter format = DateTimeFormat.forPattern(dateFormat);
		String formattedDate = format.print(date);
		return formattedDate;
	}
	public static Date convertUnixDateTime(Long dateTime){

		Date date = Date.from( Instant.ofEpochSecond(dateTime));
		return date;
	}
	public static LocalDateTime convertUnixDateTime(String dateTime,ZoneId zone){
		if(zone == null){
			zone = ZoneId.of("GMT");
		}
		LocalDateTime date = LocalDateTime.ofInstant(Instant.parse(dateTime), zone);
		return date;
	}	
	public static Date convertUnixDateTime(String dateTime){
		Long dateLong = Long.parseLong(dateTime);
		Date date = convertUnixDateTime(dateLong);
		return date;
	}
	public static Date convertStringDateTime(String date,String pattern){
		//DateTimeFormatter format = DateTimeFormatter.ofPattern(pattern);
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date dateFormatted = null;
		try {
			dateFormatted = format.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateFormatted;
	}

	public static String convertToJSON(Object o) throws IOException{
	ObjectMapper mapper = getObectMapperInstance();
		String map = null;
		if(o != null && o instanceof Object){
			map = mapper.writeValueAsString(o);
		}
		return map;
	}
	
	public static <T>  T convertFromJSON(Class<T> cls, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObectMapperInstance();
		T type = null;
		if(json != null){
			type = mapper.readValue(json,cls);
		}
		
		return type;
	}
	
	public static <T> T convertFromJson(TypeReference<T> r, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObectMapperInstance();
		T t = null;
		if(json != null){
			t = mapper.readValue(json, r);
		}
		return t;
		
	}
	
	public static String formatDateToString(Date date,String dateFormat){
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		String formattedDate = format.format(date);
		return formattedDate;
	}

	/**
	 * Get New Instance of ObjectMapper
	 * @return
	 */
	public static ObjectMapper getObectMapperInstance() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		return mapper;
	}
	
	public static Client getRESTClientInstance(){
		return RestClientProvider.getInstance().client();
	}

	public static HttpClient getRESTInstance(){
		return RestClientProvider.getInstance().getClient();
	}
	public static String padStringRight(String value,int numOfPad,String paddingChar){

		return StringUtils.rightPad(value, numOfPad,paddingChar);
	}
	public static String padStringLeft(String value,int numOfPad,String paddingChar){
		String amount = value.replaceAll(",", "");
		return StringUtils.leftPad(amount, numOfPad,paddingChar);
	}
	
	
	public static byte[] encryptPayment(String payment){
		byte[] encrypt = null;
	 
	    try{
	        MessageDigest crypt = MessageDigest.getInstance("SHA1");

	        crypt.update(payment.getBytes());
	        encrypt= crypt.digest();
	    }catch(NoSuchAlgorithmException e){
	        e.printStackTrace();
	    }
	    return encrypt;
	}

	
	public static String retrieveLastCardNumber(String cardNumber){
		String lastNumber = null;
		if((cardNumber != null && !cardNumber.isEmpty()) && cardNumber.length() > 4){
			lastNumber = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
		}
		return lastNumber;
	}

	public static String convertCheckValue(boolean eInvoice) {
		String isChecked = "";
		if(eInvoice){
			isChecked = "checked";
		}
		return isChecked;
	}

	public static boolean convertYesNoToBoolean(String option) {
		boolean result = false;
		if(StringUtils.isNotEmpty(option)){
			result = option.equalsIgnoreCase("yes") ? true : false;
		}
		return result;
	}

	public static Client getExtClientInstance() {
		// TODO Auto-generated method stub
		return RestClientProvider.getInstance().extenernalClient();
	}
	public static String generateEmployeeNumber(){
		StringBuilder sbr = new StringBuilder();
		LocalDateTime dt = LocalDateTime.now();
		
		String characters = RandomStringUtils.randomAlphanumeric(6).toUpperCase();
			sbr.append("HT");
			sbr.append(dt.getMinute());
			sbr.append(characters);
		return sbr.toString();
	}

	public static String generateSalesRepNumber(){
		StringBuilder sbr = new StringBuilder();
		LocalDateTime dt = LocalDateTime.now();
		
		String characters = RandomStringUtils.randomAlphanumeric(6).toUpperCase();
			sbr.append("AR");
			sbr.append(dt.getMinute());
			sbr.append(characters);
		return sbr.toString();
	}
	/**
	public static TupleType getTupleTypeIntText(Session session){
		return TupleType.of(session.getCluster().getConfiguration().getProtocolOptions().getProtocolVersion(),session.getCluster().getConfiguration().getCodecRegistry(),DataType.cint(),DataType.text());
	}**/
	public static Location getAgentLocation(String regionStr,String agentLocation,String googleGeoCodingKey) throws PlatformException{
		Location location = new Location();
		GeoApiContext context = new GeoApiContext().setApiKey(googleGeoCodingKey);
		context.setConnectTimeout(30, TimeUnit.SECONDS);
		GeocodingResult[] results =  null;
		try {
			String locationStr = formatEmployeeLocation(regionStr,agentLocation);
			results =  GeocodingApi.geocode(context,locationStr).await();
			location.setLatitude(results[0].geometry.location.lat);
			location.setLongitude(results[0].geometry.location.lng);
			location.setGeospatial(formattedGeoSpatial(results[0].geometry.location.lng,results[0].geometry.location.lat));
		} catch (IOException e) {
			throw new PlatformException(e);
		}catch (Exception e) {
			throw new PlatformException(e);
		}
		return location;
	}
	public static LocationDTO getAgentLocation(String agentLocation,String googleGeoCodingKey) throws PlatformException{
		LocationDTO location = null;
		GeoApiContext context = new GeoApiContext().setApiKey(googleGeoCodingKey);
		context.setConnectTimeout(30, TimeUnit.SECONDS);
		GeocodingResult[] results =  null;
		try {
			results =  GeocodingApi.geocode(context,agentLocation).await();
			if(results.length > 0){
				location = new LocationDTO();
				location.setLatitude(results[0].geometry.location.lat);
				location.setLongitude(results[0].geometry.location.lng);
				location.setGeospatial(Point.fromDegrees(location.getLatitude(), location.getLongitude()));
			}

		} catch (IOException e) {
			throw new PlatformException(e);
		}catch (Exception e) {
			throw new PlatformException(e);
		}
		return location;
	}	
	/**
	 * 
	 * @param employee
	 * @param agentLocation - Location of Agent Office and not where the agent operates
	 * @return
	 */
	private static String formatEmployeeLocation(String regionStr,String agentLocation){
		StringBuilder sbr = new StringBuilder();
		if(regionStr != null){
			String region = RegionUtils.getRegionName(regionStr);
			sbr.append(agentLocation).append(", ").append(region).append(" Ghana");
		}
		return sbr.toString();
	}
	
	private static String formattedGeoSpatial(Double lng, Double lat) throws IOException{
		String longitude = Double.toString(lng);
		String latitude = Double.toString(lat);
		AgentGeoLocation agentGeoLocation  = new AgentGeoLocation(longitude,latitude);
		String geoString = null;
		try {
			geoString = PlatformUtils.convertToJSON(agentGeoLocation);

		} catch (IOException e) {
			throw e;
		}
		return geoString;
	}

	public static Integer getEmployeeNumber() {	
			StringBuilder sbr = new StringBuilder();
			String rand = RandomStringUtils.randomNumeric(8);
			sbr.append(rand);
			
			Integer empNumber = Integer.valueOf(sbr.toString());
			return empNumber;

	}
}
