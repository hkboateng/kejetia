/**
 * 
 */
package com.protecksoftware.kejetia.pipeline.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author hkboateng
 *
 */
public class PropertyUtils {

	private static final PropertyUtils property = new PropertyUtils();
	private PropertyUtils(){}
	
	public static PropertyUtils getInstance(){
		return property;
	}
	
	/**
	 * 
	 * @param propertname
	 * @param key
	 * @throws IOException
	 */
	public String getApplicationPrperty(String propertyname, String key){
		Properties props = new Properties();
		try {
			props.load(PropertyUtils.class.getResourceAsStream(propertyname));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!props.containsKey(key)){
			return null;
		}
		return (String) props.get(key);
	}
	
	public static Properties getApplicationProperty(String propertyname){
		Properties properties = new Properties();
        try {
            String filename = (propertyname.length() > 0)
                    ? propertyname: "properties/platform.properties";

            FileInputStream fis = new FileInputStream("properties/"+filename);
            properties.load(fis);
            fis.close();

            return (properties);
        } catch (IOException ioe) {
            // do nothing.  An empty Properties object will be returned.
        }

		return properties;
	}
	
	public static String getValueFromPropertyFile(String propertyname,String propertyKey){
		String result = null;
		if(propertyname != null && propertyKey != null){
			Properties props = getApplicationProperty(propertyname);
			result = props.getProperty(propertyKey);
		}
		return result;
	}
}
