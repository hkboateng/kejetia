package com.protecksoftware.kejetia.pipeline.utils;

import java.security.KeyManagementException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.glassfish.jersey.SslConfigurator;

public class RestClientProvider {
	
	private static final RestClientProvider provider = new RestClientProvider();
	
	private RestClientProvider(){}
	
	public static RestClientProvider getInstance(){
		return provider;
	}
	public  Client extenernalClient(){
		Client client = null;
		try{
	     SslConfigurator sslConfig = SslConfigurator.newInstance()
	    	//.keyStorePassword("macintosh1".toCharArray())
	     	.keyStoreFile("/home/STAR_protecksoftware_com.keystore")
	        .keyStorePassword("macintosh1");
	     SSLContext sc = sslConfig.createSSLContext();
	     sc.init(null, certs, new java.security.SecureRandom());
	     client = ClientBuilder.newBuilder().hostnameVerifier(new TrustAllHostNameVerifier()).sslContext(sc).build();
	     
		}catch (Exception e) {
			client = null;
			e.printStackTrace();
		}
		return client;
	}	
	
	public  Client client(){
		Client client = null;
		try{
	     SslConfigurator sslConfig = SslConfigurator.newInstance()
	    	//.keyStorePassword("macintosh1".toCharArray())
	     	.keyStoreFile("/home/STAR_protecksoftware_com.keystore")
	        .keyStorePassword("macintosh1");
	     SSLContext sc = sslConfig.createSSLContext();
	     sc.init(null, certs, new java.security.SecureRandom());
	     client = ClientBuilder.newBuilder().sslContext(sc).build();
	     
		}catch (Exception e) {
			client = null;
			e.printStackTrace();
		}
		return client;
	}
    static TrustManager[] certs = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                	 return new X509Certificate[0];
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
            }
    };
    
    public static class TrustAllHostNameVerifier implements HostnameVerifier {

    	@Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }
    
    public synchronized HttpClient getClient(){
        SSLContext sslcontext = SSLContexts.createDefault();
        HttpClient client = null;
        try {
			sslcontext.init(null, certs, new java.security.SecureRandom());
			CloseableHttpClient httpclient = HttpClients.custom()
	                .setSslcontext(sslcontext)
	                .build();
			client = httpclient;
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return client;
    }
}
