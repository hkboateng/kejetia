/**
 * hkboateng
 */
package com.protecksoftware.kejetia.pipeline.utils;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author hkboateng
 *
 */
public class ValidationUtils {
	
	private static final Pattern EMAIL_PATTERN = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	
	private static final Pattern ZIPCODE_PATTERN = Pattern.compile("^[0-9]{5}(?:-[0-9]{4})?$");
			
	private static final Pattern FIRSTNAME_PATTERN = Pattern.compile("([a-zA-Z.'-])\\w+");
	
	private static final Pattern LASTNAME_PATTERN = Pattern.compile("[A-Za-z]+(\\s[A-Za-z]+)?");
	
	private static final Pattern STRINGNOSPACE_PATTERN = Pattern.compile("([a-zA-Z])\\w+");
	
	private ValidationUtils(){}
	
	/**
	 * Validate against SQL Injection
	 * @return
	 */
	public static String encode(String value){
		return (value != null) ? StringEscapeUtils.escapeJava(value.trim()) : "";
	}
	public static boolean isValid(String value){
		boolean valid = false;
		String encode = encode(value);
		if(isNullOrBlank(encode)){
			valid = false;
		}else{
			valid = true;
		}
		return valid;
	}
	//private String 
	public static boolean isAlpha(String s){

			return StringUtils.isAlpha(s);

	}

	public static boolean isAlphaNumeric(String s){
		return StringUtils.isAlphanumeric(s);
	}	
	public static boolean isNumeric(String s){
		if(isNullOrBlank(s)&& !isValid(s)){
			return false;
		}else{
			return StringUtils.isNumeric(s);
		}
	}		
	
	public static boolean isNullOrBlank(String str){
		return (StringUtils.isBlank(str) || StringUtils.isEmpty(str));
	}
	
	public static boolean isZipCodeValid(String zipcode){
		if(isNullOrBlank(zipcode) || !isValid(zipcode)){
			return false;
		}else{
			
			return ZIPCODE_PATTERN.matcher(zipcode).matches();
		}		
	}
	
	public static boolean isStringNumeric(String number){
		boolean status = false;
		if(!number.isEmpty() && StringUtils.isNumeric(number)){
			status = true;
		}
		return status;
	}
	public static boolean isEmailValid(String email){
		if(isNullOrBlank(email)){
			return false;
		}else{
			return EMAIL_PATTERN.matcher(email).matches();
		}
	}
	
	public static boolean isValidFirstName(String firstname){
		return FIRSTNAME_PATTERN.matcher(firstname).matches();
	}
	
	public static boolean isValidLastName(String lastname){
		return LASTNAME_PATTERN.matcher(lastname).matches();
	}
	
	public static boolean isValidStringWithNoSpaces(String str){
		return STRINGNOSPACE_PATTERN.matcher(str).matches();
	}

	
}
