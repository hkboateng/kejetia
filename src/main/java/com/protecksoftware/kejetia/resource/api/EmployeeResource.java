package com.protecksoftware.kejetia.resource.api;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.protecksoftware.kejetia.domain.Employee;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.domain.dto.LocationDTO;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeRequest;
import com.protecksoftware.kejetia.pipeline.businessobjects.EmployeeResponse;
import com.protecksoftware.kejetia.pipeline.services.EmployeeService;
import com.protecksoftware.kejetia.pipeline.services.SalesService;
import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;
import com.protecksoftware.kejetia.pipeline.utils.exception.PlatformException;

@Component
@Path("/api/employee")
public class EmployeeResource {
	private static final Logger logger = LoggerFactory.getLogger(EmployeeResource.class.getName());
	
	private @Value("${googleMapKey}") String googleGeoCodingKey;
	
	@Autowired
	private SalesService salesServiceImpl;
	
	@Autowired
	private EmployeeService employeeServiceImpl;
	
	@POST @Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody EmployeeResponse save(@RequestBody String request,@Suspended final AsyncResponse asyncResponse){
		EmployeeRequest employeeRequest = null;
		Employee employee = null;
		EmployeeResponse response = null;
		try {
			employeeRequest = PlatformUtils.convertFromJSON(EmployeeRequest.class, request);
			employee = employeeRequest.getEmployeeInstance();
			response = employeeServiceImpl.create(employeeRequest);
			if(response.isResult()){
				employee = employeeServiceImpl.findById(response.getEmployeeId()).getEmployee();
				response.setEmployee(employee);
				asyncResponse.resume(response);				
			}else{
				response.setMessage("Salesman could not save Employee Information..");
			}


		} catch (JsonParseException e) {
			logger.error(e.getMessage(),e);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage(),e);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
		return response;
		
	}
	
	@GET
	@Path("/getAllEmployee")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public @ResponseBody List<Employee> getAllEmployee(@Suspended final AsyncResponse asyncResponse ){
		
		logger.info("Searching for Employee information");
		List<Employee> employeeList = null;
		try{
			employeeList = employeeServiceImpl.getAll();
			asyncResponse.resume(employeeList);
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
		}
		

		return employeeList;
	}
	
	@GET
	@Path("/findEmployeeByPosition")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public @ResponseBody EmployeeResponse getEmployeeByPosition(@Suspended final AsyncResponse asyncResponse,@QueryParam("position" )String position ){
		
		logger.info("Searching for Employee information who have job title: "+position);
		EmployeeResponse employeeList = null;
		try{
			if(position.equalsIgnoreCase("insuranceAgent")){
				employeeList = employeeServiceImpl.getAllInsuranceAgent(position);
			}else{
				employeeList = employeeServiceImpl.getAllEmployeeByPosition(position);
			}
			asyncResponse.resume(employeeList);
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
		}
		

		return employeeList;
	}
	
	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody String test(){
		logger.info("Searching for Employee information");

		return "Searching for Employee information";
	}
	
	@GET
	@Path("/findEmployeById")
	@Produces(MediaType.APPLICATION_JSON)
	public EmployeeResponse findEmployeById(@Suspended final AsyncResponse asyncResponse ,@QueryParam("employeeId") String uniqueId,@QueryParam("searchType") String searchType){
		List<Employee> employeeList = null;
		EmployeeResponse response = null;
		if(searchType != null && uniqueId != null){
			if("employeeId".equalsIgnoreCase(searchType)){
				Integer employeeId = Integer.parseInt(uniqueId);
				response = employeeServiceImpl.findById(employeeId);
			}
		}
		
		
		return response;
	}
	
	@Path("/findInsuranceAgentByLocation")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody Response findAgentByLocation(@QueryParam("location") String locationstr){
		Status status = Status.OK;
		List<InsuranceAgentDTO> agentList = null;
		//InsuranceAgentResponse response = null;
		try{
			if(locationstr != null){
				LocationDTO location = PlatformUtils.getAgentLocation(locationstr, googleGeoCodingKey);
				if(location != null){
					agentList = salesServiceImpl.findAgentByLocation(location);

					if(agentList != null){
						logger.info("Search for Insurance Agents returned results size: "+agentList.size());
					}else{
						status = Status.NO_CONTENT;
					}					
				}else{
					status = Status.NO_CONTENT;
				}
			}else{
				status = Status.NOT_FOUND;
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return Response.ok(agentList).status(status).build();
	}
}
