package com.protecksoftware.kejetia.resource.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.protecksoftware.kejetia.domain.AgentProspectiveInsured;
import com.protecksoftware.kejetia.domain.dto.InsuranceAgentDTO;
import com.protecksoftware.kejetia.domain.dto.LocationDTO;
import com.protecksoftware.kejetia.pipeline.services.SalesService;
import com.protecksoftware.kejetia.pipeline.utils.PlatformUtils;

@Component
@Path("/api/sales")
public class SalesResource {
	private static final Logger logger = LoggerFactory.getLogger(SalesResource.class.getName());
	
	private @Value("${googleMapKey}") String googleGeoCodingKey;
	
	@Autowired
	private SalesService salesServiceImpl;
	
	@Path("/findInsuranceAgentByLocation")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAgentByLocation(@QueryParam("location") String locationstr){
		Status status = Status.OK;
		List<InsuranceAgentDTO> agentList = null;
		try{
			if(locationstr != null){
				LocationDTO location = PlatformUtils.getAgentLocation(locationstr, googleGeoCodingKey);
				agentList = salesServiceImpl.findAgentByLocation(location);
				if(agentList != null){
					logger.info("Search for Insurance Agents returned results size: "+agentList.size());
				}
			}else{
				status = Status.BAD_REQUEST;
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return Response.status(status).entity(agentList).build();
	}
	
	@Path("/saveAssignAgentProspectiveInsured")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveAssignAgentProspectiveInsured(HttpServletRequest request){
		Status status = Status.OK;
		AgentProspectiveInsured agentProspect = null;

		return Response.status(status).entity(agentProspect).build();
	}
	
	@Path("/test")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String test(@QueryParam("location") String locationstr){
		return locationstr;
	}
}
